/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />

interface Frontmatter {
    slug: string;
    title: string;
    pubDate: string;
    tags: string[];
    description: string;
}
