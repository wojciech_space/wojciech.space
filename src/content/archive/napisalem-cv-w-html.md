---
title: "Napisałem CV w… HTML. Ty też możesz."
description: "Potrzeba chwili zmusiła mnie do napisania jednego z ważniejszych dokumentów w życiu zawodowym — swojego Curriculum Vitae."
pubDate: 2017-05-30
tags: 
  - "Junior WebDev"
slug: "napisalem-cv-w-html"
---

Potrzeba chwili zmusiła mnie do napisania jednego z ważniejszych dokumentów w życiu zawodowym — swojego Curriculum Vitae. Jednak po przekopaniu swojej biblioteki darmowych rzeczy z [Creative Market](/archive/legalne-zrodla-zdjec-i-szablonow/) i stwierdzeniu, że nie ma tam żadnego szablonu, który podoba mi się, stworzyłem własny, po swojemu. Jedyne narzędzia: edytor i przeglądarka.

<!--more-->

## Dlaczego strona internetowa, a nie Photoshop?

Nie ukrywam, nie jestem mistrzem Photoshopa (którego najpierw trzeba mieć) lub GIMP-a. CV powstało więc dzięki temu, co umiem najlepiej — HTML i CSS są świetnymi językami, by stworzyć własne "résumé".

![Curriculum Vitae.](images/cv-723x1024.png)

Przy okazji, jest to kolejna lekcja dla juniora. Układ kolumn i stopka stoją dzięki flexboxowi, a wygląd jest napisany w SASS-ie i skompilowany dzięki ~~mojemu starterowi~~ **(UPDATE 30-08-2024: kod nie jest już dostępny)**.

Postarałem się też, żeby ten szablon był nastawiony na programistów. Dużą część zajmuje sekcja z projektami, wraz z klikalnymi w PDF-ie linkami do repozytoriów/stron i krótkimi opisami. Pod spodem całkiem sporo miejsca poświęciłem również umiejętnościom, znanym narzędziom czy językom obcym.

## Ten szablon jest dla Ciebie

Wystarczy tylko pobrać szablon ~~stąd~~ **(UPDATE 30-08-2024: kod nie jest już dostępny)**, wprowadzić zmiany i wydrukować swoją "CV-kę". Prościej chyba się nie dało 😉

Możesz też zajrzeć do repozytorium, żeby np. podejrzeć pliki lub konfigurację: **(UPDATE 30-08-2024: kod nie jest już dostępny)**.

* * *

Chciałbym w tym miejscu podziękować Andrei, które pokazało mi, że da się coś takiego zrobić (bo samo to zrobiło) oraz dwóm recenzentom mojej pracy: Natalii i Michałowi. Andrea, reklamę masz już wykupioną w repo, tutaj nie dostaniesz.
