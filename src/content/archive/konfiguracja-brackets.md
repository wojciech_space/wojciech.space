---
title: "Konfiguracja Brackets i przydatne wtyczki"
description: "Podstawową dobrego workflow podczas pracy z web developmentem jest dobry edytor tekstowy bądź IDE. Dzisiaj pokażę Ci, jak dobrze skonfigurować Brackets — jeden z najlepszych edytorów open source na świecie."
pubDate: 2017-05-16
tags: 
  - "Junior WebDev"
slug: "konfiguracja-brackets"
---

Podstawową dobrego workflow podczas pracy z web developmentem jest dobry edytor tekstowy bądź IDE. Dzisiaj pokażę Ci, jak dobrze skonfigurować Brackets — jeden z najlepszych edytorów open source na świecie.

<!--more-->

## 0\. Instalacja Chrome

Jeśli jeszcze z jakiegoś powodu nie masz zainstalowanej przeglądarki Chrome, zrób to teraz. Nie dość, że jest w mojej opinii najlepsza do pisania stron i programowania, to dzięki Brackets jest w stanie pokazywać zmiany na stronie na żywo dzięki wbudowanej w edytor funkcji _Live Preview_.

## 1\. Instalacja Brackets

Instalacja przebiega standardowo, więc nie będę się nad nią zbyt długo rozwodzić. Jedyne, o czym należy pamiętać, to pozostawienie zaznaczonej opcji _Add "Open with Brackets" to Explorer …_, dzięki czemu wszelkie pliki i foldery będziemy mogli otworzyć w programie z poziomu Eksploratora plików.

![Instalator Brackets](images/brackets-instalacja.png)

## 2\. Motywy

Na samym początku polecam od razu zmienić motyw na ciemny (`Menu → Widok → Motywy`). Dzięki ciemnemu motywowi wzrok mniej się męczy oraz pozwala pracować dłużej i w nocy, co nie oznacza, żebyśmy nie robili sobie przerw ;) Nowe motywy można znaleźć m.in. we wbudowanym Menedżerze rozszerzeń (`Menu → Plik → Menedżer rozszerzeń`) lub wpisując frazę _brackets themes_ w Google.

## 3\. Wtyczki

Wtyczki można zainstalować przez Menedżer rozszerzeń — bezpośrednio z menedżera, przez paczkę ZIP lub podając adres URL do niej. Poniżej przygotowałem listę wtyczek, które ułatwią Ci pracę z Brackets i aktualnymi projektami:

### Emmet

Emmet jest jednym z najpopularniejszych rozszerzeń do webdevu we wszystkich edytorach oraz IDE. Pozwala on na pisanie skrótów selektorów, które automatycznie są konwertowane na pełny kod. Najlepiej obrazuje to poniższy GIF:

![Emmet](images/emmet.gif)

Instalacja: wpisz w Menedżerze rozszerzeń _Emmet_ i zainstaluj wtyczkę. Użycie: wpisz skróty selektorów, jakich chcesz użyć i naciśnij `[Tab]`. Pełna lista skrótów: [Emmet Cheat Sheet](https://docs.emmet.io/cheat-sheet/)

### Beautify

Twój kod bardziej przypomina krzak niż pięknie poukładany ciąg linii z odpowiednimi wcięciami? Nic nie szkodzi, Beautify poukłada za Ciebie tabulacje i entery. Tylko następnym razem postaraj się napisać taki kod, do którego nie będzie potrzeba poprawy wielu wierszy ;)

Instalacja: wpisz w Menedżerze rozszerzeń _Beautify_ i zainstaluj wtyczkę. Użycie: użyj kombinacji klawiszy `[Ctrl]+[Alt]+[B]` lub użyj menu dostępnego pod prawym przyciskiem myszy.

## Brackets File Icons

Brackets domyślnie pokazuje rozszerzenia plików na liście po lewej. Wygodniej jednak jest spojrzeć na ikonę, która powie nam szybciej, z jakim rodzajem pliku teraz pracujemy.

![Brackets File Icons](images/brackets-icons-1024x527.png)

Instalacja: wpisz w Menedżerze rozszerzeń _Brackets File Icons_ i zainstaluj wtyczkę.

### Brackets Git

Brackets Git ma szerokie zastosowanie przy pracy z Gitem. Rozszerzenie pokazuje które pliki oraz linie kodu zostały zmienione od ostatniego commitu, pozwala na commit i push zmian, zmianę gałęzi projektu, etc.

![Brackets Git](images/brackets-git-zmiany-1024x573.png)

Instalacja: wpisz w Menedżerze rozszerzeń _Brackets Git_ i zainstaluj wtyczkę. Użycie: panel otwierany jest za pomocą ikony na pasku po prawej stronie lub za pomocą kombinacji klawiszy `[Ctrl]+[Alt]+[G]`.

### Fira Code

Fira Code to font typu monoscape, zaprojektowany do używania w edytorach dla programistów. Jego największą zaletą jest graficzna "ludzkie" podejście do wyświetlania znaków równości, porównań czy większości/mniejszości:

![FiraCode JavaScript](images/firacode-javascript-1024x416.png)

Instalacja: pobierz [paczkę ZIP](https://github.com/tonsky/FiraCode/archive/master.zip) z fontem i zainstaluj go do systemu. Następnie zainstaluj przez Menedżer rozszerzeń w Brackets wtyczkę _Fira code in Brackets_ — wpisz _fira_ i zainstaluj rozszerzenie autorstwa Paula de Rosanbo. W opcjach motywu (`Menu → Widok → Motywy`) wpisz w polu Rodzina czcionek _'Fira Code', monoscape_.

### Color Highlighter

Color Highlighter to prosta wtyczka do CSS, LESS, SCSS, SASS i Stylus, która koloruje użyte w pliku wartości lub słowa kluczowe na kolory, które sobą reprezentują.

![Brackets Color Highlighter](images/brackets-color.png)

Instalacja: wpisz w Menedżerze rozszerzeń _Color Highlighter_ i zainstaluj wtyczkę.

* * *

Tak skonfigurowany Brackets pozwala na szybką komfortową pracę z kodem.
