---
title: "Microsoft nienawidzi logicznego myślenia (i Polski)"
description: "Pozostanę dzisiaj w świecie technologii, jednakże będzie to osobisty rant na Microsoft i ich system operacyjny na smartfony. Przygotujcie popcorn i chusteczki, bo to będzie śmiech przez łzy."
pubDate: 2017-06-20
tags: 
  - "Przemyślenia"
  - "Technologie"
slug: "microsoft-nienawidzi-logiki"
---

Pozostanę dzisiaj w świecie technologii, jednakże będzie to osobisty rant na Microsoft i ich system operacyjny na smartfony. Przygotujcie popcorn i chusteczki, bo to będzie śmiech przez łzy.

<!--more-->

## Konfiguracja specjalnej troski

Zrobiłem hard reset. Liczba aplikacji do zaktualizowania: 38. Okej, rozumiem, zostawię na noc, jak wstanę, to wszystko będzie gotowe. Poranek dnia następnego, liczba aplikacji do zaktualizowania: 38. Przy zablokowanym ekranie Windows nie zaktualizował niczego. Żenadłem srogo i wybrałem w ustawieniach opcję, by ekran się nie wyłączał w ogóle. Zadziałało, szkoda, że tak późno.

## Używasz znaków narodowych? Masz przesrane

… przynajmniej w kwestii ustawień. My, Polacy, lubimy korzystać z ogonków. Przynajmniej ja sobie tego życzę. Przedstawię ścieżkę, jaką użytkownik musi **samodzielnie odkryć** i przebyć, by ustawić kodowanie znaków narodowych w SMS-ach:

`Ustawienia → Dodatki → Usługi sieciowe → Zestaw znaków SMS`.

Gdy już wybierzemy interesującą nas opcję "Pełny"… musimy zresetować telefon. Android ma to opanowane w kilka kliknięć w ustawieniach, które są logiczne, i nie wymaga to restartu.

## Jak spieprzyć użytkownikowi sen?

Na przykład nie pozwalając mu korzystać z trybu "Nie przeszkadzać". Oczywiście, mogę zawsze ustawić tryb dzwonka na same wibracje, ale nie o to chodzi. Chcę dostawać powiadomienia, gdy zadzwoni rodzina i najbliżsi, ale wibracje telefonu leżącego trzy metry ode mnie nie obudzą.

Więc czemu nie mogę korzystać z trybu "Nie przeszkadzać"? Ponieważ jest połączony z Cortaną, a tej asystentki głosowej, jak zresztą tych od Google czy Apple, nie ma w Polsce. Nie masz Cortany? Ups, ale możesz się walić.

Zmieńmy region na UK i język na angielski. Jako budzika używam aplikacji Realarm, która powoli wybudza mnie narastającym dźwiękiem budzika i nie pozwala, żebym znowu zasnął, zmuszając mnie do rozwiązania działania, by wyłączyć budzik. Ustawiam godziny ciszy od 23 do 6. Mam budzik na 5:30. Tak, już zgadliście — Realarm nie przebije się przez godziny ciszy, nie obudzi mnie. API umożliwiające omijanie godzin ciszy przez budziki istnieje w wersji stabilnej dopiero od kwietnia 2017, półtora roku po premierze systemu. Gratulacje, szybko wam poszło.

## "Nie kocham swoich spółek-córek"

Microsoft ma w swoim portfolio bogatą ilość zależnych spółek, między innymi LinkedIn czy SwiftKey. Dla niezorientowanych, pierwsze to Facebook dla pracowników i pracodawców, drugie to (w mojej opinii) producent najlepszej klawiatury na Androida. Co dobrego wynikło z fuzji tych dwóch firm z gigantem? Absolutnie NIC.

Przez ostatnie parę tygodni korzystałem z Androida, taka odskocznia od codzienności z Lumią. Smartfona zwróciłem odpowiedniej osobie, hard reset mojej 532, instalacja apek… Nie ma LinkedIn. Tak, MS wykastrował tę aplikację ze swojego sklepu. Pomimo tego, że działała całkiem dobrze. Nie ma i już.

Co do SwiftKey, MS kupił potężną technologię, potężne zasoby. Czy zaimplementował je w choć jednym procencie? Oczywiście, że nie. Swype działa momentami tragicznie, olewa to, co chciałem napisać i wstawia jakieś losowe słowa, które według systemu są jedynymi słusznymi w jego ocenie.

Język polski? Zapomnij. Prędzej niemowa nauczy się polszczyzny, niż zaawansowane algorytmy zrozumieją, że nasz alfabet nie został wykastrowany z "ogonków" lub kresek nad literami. Notoryczne "tez" zamiast "też", brak nauki kontekstów zdania, przez co często muszę poprawiać ostatni znak lub kilka, by przypadki się zgadzały… A wystarczy stado wyszkolonych małp, żeby przeportować odpowiednią wersję na mobilne okienka.

## Producent olał, programista też

Nie muszę raczej tłumaczyć, dlaczego mamy tak słabe aplikacje na Windows 10 Mobile. Microsoft olał platformę, więc programiści zrobili dokładnie to samo. Toporne aplikacje, liczne błędy, zamykanie się apek, brak integracji… À propos, dopiero po roku odkryłem aplikację, która ma zintegrowane logowanie z Facebookiem przez aplikację, a nie przez przeglądarkę. Bynajmniej nie Messenger — to OLX.

Tutaj na chwilę zatrzymajmy się przy mojej "wielkiej trójcy" aplikacji: Facebook, Messenger, Twitter. O ile programiści Marka Cukiergóry pomyśleli i przeportowali wersję z iOS, o tyle zespół "ćwierkacza" zlekceważył temat. Osobiście nie dziwię się, ale przez to też się wkurzam. Facebook i Messenger są ze sobą połączone tylko w jednym miejscu — gdy trzeba wysłać komuś wiadomość. Logowanie do obydwu są oddzielne, nie ma takiego połączenia jak na Androidzie lub Japku.

Kochany Twitterze, co zrobiła ci literka "ż"? Dlaczego jak wpiszę z klawiatury, dajmy taki przykład, "jeż", to pozostaje mi jedynie "j"? "Ż" działa jak Backspace. Nie żartuję. Muszę tę literę wprowadzać ze Swype'a (o ile klawiatura zrozumie, co chciałem wpisać).

## Nie polecam, 2/10

Lumię 532 jako smartfon lubię. Taki mały, gruby, ale świetnie mi leży w dłoni i spełnia swoje podstawowe funkcje, zadzwonię i wyślę SMS-a bez żadnych problemów. To system jest zły. Nielogiczny, odcinający się od użytkowników, których ojczysty język jest nie w smak Cortanie, zaniedbany, przez co programiści też się od niego odcinają. Ten osobisty wpis traktuję jako przestrogę przed zakupem nowego smartfona i z całą pewnością będzie to albo Android, albo iPhone. Dla mnie przygoda z Windows 10 Mobile powoli i na szczęście dobiega końca.
