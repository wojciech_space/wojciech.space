---
title: "Legalne źródła zdjęć i szablonów"
description: "Ciekawe zdjęcia do wpisu czy nowy motyw do bloga nie muszą być drogie — oto kolejne, legalne i darmowe źródła materiałów."
pubDate: 2017-05-02
tags: 
  - "antypiractwo"
  - "Junior WebDev"
slug: "legalne-zrodla-zdjec-i-szablonow"
---

Ciekawe zdjęcia do wpisu czy nowy motyw do bloga nie muszą być drogie — oto kolejne, legalne i darmowe źródła materiałów.

Po wpisie o [legalnych źródłach książek i kursów](/archive/legalne-zrodla-ksiazek-i-wiedzy/) nadszedł czas na część drugą, tym razem zajmę się tematem zdjęć i szablonów HTML.

<!--more-->

## Darmowe i legalne zdjęcia

### Unsplash

![Unsplash - zdjęcie z serwisu](images/unsplash-1024x566.png)

Unsplash to moje ulubione miejsce z darmowymi zdjęciami. Sam wielokrotnie korzystałem w projektach właśnie stamtąd. Na podstawie API stworzono usługę hostowania zdjęć z serwisu jako placeholderów — unsplash.it (nie mylić ze źródłem: unsplash.com)

- blisko 200 tysięcy zdjęć
- licencja CC0

Linki: [Unsplash](https://unsplash.com), [usługa hostowania placeholderów](https://unsplash.it), [zdjęcie na zrzucie ekranu](https://unsplash.com/@wexor?photo=L-2p8fapOA8)

### Pixabay

![Pixabay](images/pixabay-page-1024x457.png)

Pixabay to największy serwis w zestawieniu. Oprócz samych zdjęć twórcy udostępniają tutaj również ilustracje, grafiki wektorowe oraz materiały wideo.

- ponad 930 tysięcy zdjęć, ilustracji, grafik wektorowych i wideo
- licencja CC0

Linki: [Pixabay](https://pixabay.com/pl/)

### Pexels

![Pexels](images/pexels-1024x270.png)

Pexels na pewno nie imponuje rozmiarem biblioteki, zdjęć jest ledwo ponad 30 tysięcy. Serwis trafił do zestawienia za rozszerzenia, które oferuje (oficjalne bądź firm trzecich). Dedykowana aplikacja dla systemów Windows i macOS czy wtyczka do Photoshopa są idealnymi rozwiązaniami do szybkiego tworzenia prezentacji bądź projektów. Miłym dodatkiem jest dodatkowa baza z materiałami wideo.

- ponad 30 tysięcy zdjęć i wideo
- aplikacja dla systemów Windows i macOS, wtyczka do Photoshopa i PowerPointa, receptura IFTTT dla Androida
- licencja CC0

Linki: [Pexels](https://www.pexels.com/), [rozszerzenia Pexels](https://www.pexels.com/pro/), [Pexels Videos](https://videos.pexels.com/)

### Google Grafika

![Google Grafika - wybór licencji](images/google-grafika.png)

Let me Google that for you — najpopularniejsza wyszukiwarka świata potrafi wyszukać zdjęcia z licencjami Creative Commons. Polecam wejść na stronę, na której dane zdjęcie się znajduje i sprawdzić szczegóły (na przykład czy możemy wykorzystać dane zdjęcie komercyjnie).

- ∞ zdjęć
- różne licencje

## Darmowe i legalne szablony

### Creative Market

![Creative Market](images/creative-market-1024x294.png)

Creative Market jest platformą do sprzedaży swoich szablonów, motywów, zdjęć, grafik lub innych prac i dodatków. Ceny zaczynają się od $2 za licencję. Za mocną CM stronę uznaję możliwość synchronizacji swoich zakupów bezpośrednio do Dropboxa.

Co tydzień udostępnianych jest 6 darmowych produktów. Najczęściej są to grafiki lub zdjęcia, rzadziej pojawiają się szablony i motywy. Dzięki "Free Goods" mam m.in. grafikę, która pełni rolę tła w nagłówku bloga i na portalach społecznościowych (linki znajdują się po prawej 😉).

- ponad płatnych 1,2 miliona produktów (szablony, motywy, zdjęcia, grafiki, czcionki, dodatki)
- różne licencje
- 6 darmowych produktów co tydzień

Linki: [Creative Market](https://creativemarket.com/), [Free Goods](https://creativemarket.com/free-goods)

## Start Bootstrap

![Start Bootstrap](images/start-bootstrap-1024x471.png)

Start Bootstrap to baza darmowych motywów i szablonów opartych o framework Bootstrap. Część motywów oprócz wersji statycznych ma przygotowane przez społeczność wersje dla CMS-ów. Osobiście polecam je ze względu na czysty, przejrzysty kod, co jest bardzo pomocne w przypadku szablonów.

- ok. 40 motywów i szablonów
- licencja MIT

Linki: [Start Bootstrap](https://startbootstrap.com/)
