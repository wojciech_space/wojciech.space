---
title: "Legalne źródła książek i wiedzy"
description: "Jestem jedną z tych osób, które oprócz praktycznego podejścia do tematu programowania, czyli nauki przez kodowanie i pisanie, lubią też czytać na ten temat, czy to artykuły lub poradniki, czy również książki."
pubDate: 2017-04-18
tags:
  - "Junior WebDev"
slug: "legalne-zrodla-ksiazek-i-wiedzy"
---

Jestem jedną z tych osób, które oprócz praktycznego podejścia do tematu programowania, czyli nauki przez kodowanie i pisanie, lubią też czytać na ten temat, czy to artykuły lub poradniki, czy również książki. Nie musimy jednak wydawać fortuny na naszą edukację – wystarczy chwila poszukiwań i odnajdujemy (niemal) darmowe źródła wiedzy i co najważniejsze – legalne.

<!--more-->

Jeśli nie jesteś zainteresowany programowaniem, to nic nie szkodzi, bo część przedstawionych przeze mnie źródeł zawiera również książki dla zafascynowanych fantastyką, komiksami, kryminałami etc., dla każdego coś dobrego :)

## Książki

### Packt: Free Learning - Free Technology eBooks

![Baner reklamowy PacktPub Free Learning](images/FL-HeaderBanner.png)

Packt to z najbardziej znanych wydawców książek na temat programowania, przede wszystkim dotyczącego web developmentu. Oferują oni jednego e-booka dziennie w programie "[Free Learning - Free Technology eBooks](https://www.packtpub.com/packt/offers/free-learning)". Zakres "rozdawanych" pozycji jest ogromny – od webdevu i programowania gier, przez robotykę (Arduino), aż do hakowania sieci na Kali. Wystarczy zarejestrować się i codziennie odbierać nową książkę na swoją półkę, skąd można ją pobrać (praktycznie wszystkie są w formacie PDF, EPUB i MOBI) lub czytać on-line w specjalnie przygotowanym czytniku Packtu.

### Humble Bundle

![Logo Humble Bundle](images/Humble_Bundle_logo-1024x171.png)

Część z Was może kojarzyć Humble Bundle już z promocyjnych okazji na gry. Zauważyliście jednak, że oprócz gier i miesięcznej subskrypcji jest też [zakładka z książkami](https://www.humblebundle.com/books)? Często pojawiają się tam książki związane z programowaniem. Na ten moment (początek kwietnia 2017) można w formule "Pay What You Want" (od $1) kupić [zestaw co najmniej 4 książek związanych z Pythonem](https://www.humblebundle.com/books/python-book-bundle). Co tydzień lub dwa następuje rotacja ofert, najczęściej można tam spotkać komiksy, fantastykę lub programistyczne właśnie. Przy okazji wspieramy również organizacje charytatywne - my się cieszymy ze zdobywania wiedzy, a potrzebujący z pomocy, którą dzięki nam mogą otrzymać.

### Legimi (wybrane miasta w Polsce)

![Logo Legimi](images/biglogo.png)

Osoby korzystające z miejskich bibliotek m.in. w Szczecinie, Wrocławiu, Gdańsku, Sopocie i Bydgoszczy mogą udać się z ważną kartą biblioteczną po kod, dzięki któremu mogą aktywować miesięczny lub roczny dostęp do bazy e-booków udostępnionych przez Legimi – aż 18 tysięcy pozycji. Prawdziwa perełka czytelnictwa.

## Kursy

### Udemy (#kursyudemy)

![Logo Udemy](images/logo-green.png)

[Udemy](https://www.udemy.com/) to jedna z największych platform e-learningowych na świecie. W ofercie znajdziemy ponad 45 tysięcy tutoriali wideo z zakresu programowania, rozwoju osobistego, nauki języków, fotografii etc. Nie przejmujcie się wysokimi cenami, większość z nich jest najczęściej w przedziale 5-15€. Jest jeszcze sposób, by znaleźć je za darmo.

Na Wykopie znajduje się specjalny tag autorski poświęcony kursom na Udemy – [#kursyudemy](http://www.wykop.pl/tag/kursyudemy/). Warto tam zaglądać **codziennie**, żeby nie przegapić okazji na dobry poradnik za darmo (promocje i kody rabatowe są ograniczone czasowo). Nie musicie się rejestrować, wystarczy wrzucić stronę do zakładek i regularnie tam zaglądać.

### Wes Bos (#JavaScript30, What The Flexbox?)

![Baner reklamowy #JavaScript30](images/JS3-social-share-1024x487.png)

Wes Bos jest kanadyjskim full stack web developerem. Stworzył dwa ważne dla każdego web deva kursy dotyczące JavaScriptu i nowości w CSS – flexboxa.

- [**#JavaScript30**](https://javascript30.com/) to trzydziestodniowy tutorial dotyczący czystego JavaScriptu. Nie ma tam ani grama jQuery lub innych bibliotek, są za to świetne rozwiązania związane z ES6, designem stron lub zabawą z urządzeniami mobilnymi.
- [**What The Flexbox**](https://flexbox.io/) zawiera 20 filmów dotyczących flexboxa. Pierwsza część dotyczy podstawowych zagadnień, wyjaśnia i pokazuje pojęcia, w drugiej znajdziemy praktyczne zastosowanie w codziennej pracy z CSS-em.

Oba te kursy to świetna podstawa dla juniora, dzięki którym zdobędzie doświadczenie i pozna nowe funkcjonalności JS-a i CSS-a. Polecam je każdemu początkującemu devowi.

## Post scriptum

Zachęcam do korzystania z legalnych źródeł. Książki, kursy czy inne zasoby w dzisiejszych czasach są łatwo dostępne i przede wszystkim tanie. Tak, jak przemija już piractwo muzyki czy filmów (dziękuję pan Spotify i Netflix), tak samo powinno się stać z piraceniem książek. Blisko dwa miliony osób w Polsce dzięki Legimi może nic nie płacić , by mieć dostęp do 18 tysięcy książek, a każdy z nas na Humble Bundle za jednego dolara może kupić 4 e-booki. Nie bądź cwaniak, daj piątaka lub idź do biblioteki.


* * *

Źródła: [baner reklamowy PacktPub](https://www.packtpub.com/packt/offers/free-learning), [logo Humble Bundle](https://commons.wikimedia.org/wiki/File:Humble_Bundle_logo.png), [logo Legimi](http://www.legimi.pl/czytnik/images/biglogo.png), [logo Udemy](https://www.udemy.com/staticx/udemy/images/v5/logo-green.svg), [baner reklamowy #JavaScript30](https://javascript30.com/images/JS3-social-share.png)
