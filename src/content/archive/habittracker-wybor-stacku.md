---
title: "#HabitTracker — wprowadzenie, wybór stacku technologicznego"
description: "Cześć! Rozpoczynam tym wpisem opisywanie mojej przygody z Reactem, którego uczę się obecnie w pracy i prywatnie."
pubDate: 2017-07-25
tags: 
  - "Habit Tracker"
  - "Junior WebDev"
slug: "habittracker-wybor-stacku"
---

Cześć! Rozpoczynam tym wpisem opisywanie mojej przygody z Reactem, którego uczę się obecnie w pracy i prywatnie. W domowym zaciszu zaplanowałem, że w ramach nauki stworzę aplikację typu "habit tracker", czyli do śledzenia swoich nawyków, zarówno tych pozytywnych, jak i negatywnych.

<!--more-->

Ważną częścią tej nauki będzie dla mnie też dokumentowanie jej. Oprócz odpowiedniej dokumentacji kodu aplikacji będę też tworzyć wpisy na blogu, w których będę opisywać postępy pracy oraz biblioteki, narzędzia, języki etc., z których korzystam, by jeszcze lepiej je zrozumieć. Będzie to długa podróż, a efektem finalnym, mam nadzieję, będzie piękna aplikacja webowa, dostępna dla wszystkich w ramach open source.

Zapraszam do śledzenia mojej pracy nie tylko na moim blogu, lecz również na GitHubie i Twitterze, gdzie aktualizacje będą pojawiać się najczęściej :)

## Wybór stacku technologicznego

Z racji tego, że buduję aplikację webową, będę korzystać z podstawowych języków dla webu: HTML, CSS i JavaScript. Arkusze styli będę pisać w SASS-ie, a skrypty będą zgodne ze składnią ES6, o co zadbam dzięki ESLint. Przez użycie ES6, dla zachowania pełnej kompatybilności między przeglądarkami, użyję kompilatora Babel, który przetworzy skrypty do wersji JavaScriptu zrozumiałej dla wszystkich interpreterów.

Moim podstawowym celem jest nauka Reacta, więc również on będzie jedną z podstaw tego projektu. Aby React mógł działać, użyję Node.js, paczki rzecz jasna będę instalować przez npm.

Druga główna rzecz, której chcę się nauczyć, to Webpack — dzięki niemu będę budować zminimalizowane wersje produkcyjne oraz korzystać z Reacta i jego modułów do ładowania m.in. styli.

Wiem już, że na stronie znajdą się formularze, więc do ich obsługi użyję Reduxa w wersji dedykowanej Reactowi. Dane będę przechowywać w bazie MongoDB, a do komunikacji z nią posłużę się Mongoose.

Chcę, aby moja aplikacja miała dedykowane API. Do jego stworzenia użyję Express.js.

Do obsługi logowania i rejestracji użytkowników użyję Passport.js.

Sprawą otwartą pozostaje na razie biblioteka, której użyję do tworzenia wykresów. Na tym etapie nie jest konieczne wskazanie jaka zostanie wykorzystana w projekcie.

Ogółem oto stack, którego użyję w aplikacji:

- **języki:** HTML5, CSS3, SASS, ES6
- **narzędzia:** Node.js, npm, Webpack, MongoDB, ESLint, Babel
- **biblioteki:** React, Redux, Express, Passport
