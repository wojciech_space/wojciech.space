---
title: "Gulp vs Grunt — porównanie task runnerów"
description: "Automatyzacja pracy w web developmencie to jeden z ważniejszych tematów poruszanych przez programistów w ostatnim czasie."
pubDate: 2017-07-18
tags: 
  - "Junior WebDev"
  - "Recenzje"
slug: "gulp-grunt-porownanie"
---

Automatyzacja pracy w web developmencie to jeden z ważniejszych tematów poruszanych przez programistów w ostatnim czasie. Na przełomie czerwca i lipca pochyliłem się nad tym zagadnieniem i skonfigurowałem dwa task runnery: [gulp](/archive/konfiguracja-gulp/) oraz [grunt](/archive/konfiguracja-grunt/). Przyszła pora na porównanie obu narzędzi.

Przypomnę jak były skonfigurowane task runnery na potrzeby pracy z kodem i tego porównania.

- kompilacja arkusza styli z SCSS do CSS
- łączenie plików JavaScript w jeden plik
- minimalizacja arkusza styli i skryptów
- optymalizacja zdjęć (kompresja)

## Zależności

Na początku porównajmy jak dużo miejsca oraz zależności potrzebowały oba task runnery.

- gulp: 8 zależności, 28.3 megabajtów
- **grunt**: 7 zależności, 17.1 megabajtów

Grunt dzięki wbudowanej możliwości zmiany nazwy plików "zaoszczędził" jedną paczkę. Gdyby `watch` byłby wbudowany w narzędzie, całość byłaby jeszcze lżejsza.

Na niekorzyść gulpa w tej kategorii wpłynęła konieczność użycia `plumber`, dzięki któremu działa `gulp-uglify`.

## Ilość zadań

- **gulp**: 5 zadań
- grunt: 6 zadań

Pierwsza kategoria "za" dla gulpa. Udało stworzyć się jedno zadanie mniej dzięki jednoczesnej kompilacji i minimalizacji arkuszy styli.

* * *

Chciałbym przejść teraz od ocen obiektywnych do ocen subiektywnych. Zapraszam do dyskusji w komentarzach na temat Waszych opinii na temat narzędzi, które opisuję.

## Dokumentacja

**Zwycięzca: grunt**. Twórcy przyłożyli się do tworzenia dokumentacji, która jest równocześnie szczegółowa i przystępna. [\[LINK\]](https://gruntjs.com/getting-started)

Twórcy gulpa ograniczyli się do opisu API oraz CLI w dwóch dokumentach dostępnych w repozytorium kodu. [\[LINK\]](https://github.com/gulpjs/gulp/blob/master/docs)

## Intuicyjność

**Zwycięzca: gulp**. Moim zdaniem tworzenie zadań jest prostsze w gulpie. Jest to zasługą pipe'a — dzięki niemu widzimy wewnątrz taska jakie funkcje i w jakiej kolejności zostaną użyte.

W gruncie, przy minimalnej konfiguracji, w której ograniczamy się do ustalania tasków wewnątrz funkcji inicjującej narzędzie, czasami trudno się połapać. Szczególnie początkujący programista może się zagubić, na przykład widząc raz `dist` jako samodzielny obiekt w konfiguracji, a raz jako właściwość innego obiektu.

## Skala trudności

**Zwycięzca: gulp**. Tak samo intuicyjny, jak i łatwy w opanowaniu, ponieważ jest przewidywalny. Zaczynasz od `gulp.src`, kończysz na `gulp.dest`. Składnia jest łatwa do zapamiętania, przy dłuższym użytkowaniu wpisywałem ją z palca.

Grunt wymaga ślęczenia nad dokumentacją każdej paczki, by ją skonfigurować. Jak wspominałem w kategorii "Intuicyjność", sam `dest` może być inny w zależności jak został zaimplementowany w danym pakiecie.

## Szybkość

**Remis**. Nie zauważyłem znacznych różnic w działaniu obu narzędzi. Z porównywalną prędkością wykrywają zmiany w plikach i uruchamiają zadania, które również są wykonywane z podobną szybkością.

* * *

## Zwycięzca

**Gulp**. Za łatwą konfigurację, poziom trudności i intuicyjność oraz pomimo braku rozbudowanej dokumentacji, uznaję gulpa za świetne narzędzie, które polecam do automatyzacji pracy w web developmencie.

Dzięki swoim właściwościom gulp stał się moim podstawowym narzędziem w pracy z kodem. [Stworzyłem nawet swój starter](/archive/gulp-starter/) na jego bazie, by mieć od razu solidną podstawę w nowych projektach. Zobacz również kod `gulp-starter` na ~~GitHubie~~ **(UPDATE 30-08-2024: kod nie jest już dostępny)**. A może Ty też stworzysz jakiś projekt na tym starterze? ;)

Ten wpis jest jedną z części mini serii dotyczącej task runnerów. Zobacz pozostałe wpisy:

1. [Gulp — podstawowa konfiguracja task runnera](/archive/konfiguracja-gulp/)
2. [Grunt — podstawowa konfiguracja task runnera](/archive/konfiguracja-grunt/)
3. **Gulp vs Grunt — porównanie task runnerów**
