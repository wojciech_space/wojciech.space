---
title: "TGIF — ile pozostało do weekendu?"
description: "To pytanie zadawałem sobie dosyć często. A że jestem przede wszystkim front-end web developerem, wykorzystałem ES6 i Moment.js, by stworzyć licznik, który powie Ci \"czy to już piąteczek?\"."
pubDate: 2017-08-15
tags: 
  - "Junior WebDev"
  - "Technologie"
slug: "tgif"
---

To pytanie zadawałem sobie dosyć często. A że jestem przede wszystkim front-end web developerem, wykorzystałem ES6 i Moment.js, by stworzyć licznik, który powie Ci "czy to już piąteczek?".

<!--more-->

Działanie mojego odliczania jest dosyć proste. Przy pierwszym wejściu na stronę aplikacja pyta o której kończysz pracę w piątek. Na podstawie tej godziny wylicza następnie czas pozostały do weekendu.

![](images/tgif-ask.png)

![](images/tgif-countdown.png)

W każdej chwili użytkownik może zmienić godzinę końca tygodnia roboczego. Dostępne są trzy wersje językowe: polska, angielska i niemiecka (za tłumaczenie tej ostatniej dziękuję Andrei z bloga [Avris.it](https://avris.it)).

Kod źródłowy jest dostępny na najbardziej liberalnej licencji wymyślonej w dziejach ludzkości — WTFPL (Do What the Fuck You Want Public License). Także śmiało, częstujcie się tym wszyscy, _róbta co chceta_ 😉

A co się dzieje, gdy odliczanie dojdzie do zera? Póki co nie jest to jakoś zbyt mocno ekscytujące… **(UPDATE 30-08-2024: tu był tweet ze skasowanego konta)** ale być może w wersji 2.0.0 będzie coś ekstra 😉

PS. Właściciele Lumii narzekający na Times New Roman — co mogę poradzić, że Microsoft nie stanęło na wysokości zadania i ustawiło taki fallback dla fontu monospace? ¯\\\_(ツ)\_/¯

* * *

~~APLIKACJA JEST DOSTĘPNA TUTAJ:~~ **(UPDATE 30-08-2024: aplikacja nie jest już dostępna)** Kod źródłowy: [github.com/wojciech-space/tgif](https://github.com/WojtekWernicki/tgif)

