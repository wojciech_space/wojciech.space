---
title: "#HabitTracker — szkielet projektu"
description: "Siema! W zeszłym tygodniu pisałem jakich języków, bibliotek i narzędzi użyję w projekcie Habit Tracker. Dzisiaj część druga, w której piszę o tym od czego zacząłem budować projekt."
pubDate: 2017-08-01
tags: 
  - "Habit Tracker"
  - "Junior WebDev"
slug: "habittracker-szkielet-projektu"
---

Siema! W zeszłym tygodniu pisałem [jakich języków, bibliotek i narzędzi użyję](/archive/habittracker-wybor-stacku/) w projekcie Habit Tracker. Dzisiaj część druga, w której piszę o tym od czego zacząłem budować projekt.

<!--more-->

## Trudny początek

Po kilku nieudanych próbach postawienia obok siebie Expressa oraz ręcznie skonfigurowanego Webpacka dla Reacta, postanowiłem zasięgnąć głębszej lektury dokumentacji oraz poradników w Internecie.

Z pomocą przyszedł [tekst z bloga Dave'a Ceddia](https://daveceddia.com/create-react-app-express-production/), który pokazał mi połączyć ze sobą Express oraz Reacta utworzonego dzięki kreatorowi dostarczonemu przez Facebooka — `create-react-app`. Express służy do serwowania statycznych plików (tych już po buildzie) oraz jako serwer API.

Na środowisku deweloperskim za pomocą proxy, a w środowisku produkcyjnym bezpośrednio, możemy dzięki temu w aplikacji reactowej wysyłać żądania (póki co GET), które będą przetwarzane za pomocą czystego JS-a w funkcji `fetch()`.

<script src="https://gist.github.com/wojciech-space/17775d56f3b748e62eaf9386ce7dbaf0.js"></script>

## Planowanie

Moje plany na najbliższy tydzień:

- wykonanie routingu serwera API dla wszystkich zapytań, które znajdą się w aplikacji
- podłączenie MongoDB do serwera API i zwracanie przy zapytaniach GET danych z bazy
- wykonanie routingu wewnątrz Reacta za pomocą `react-router-dom` dla strony głównej aplikacji i listy wszystkich zadań

Moją pracę możecie śledzić w repozytorium na ~~GitHubie~~ **(UPDATE 30-08-2024: kod nie jest już dostępny)**
