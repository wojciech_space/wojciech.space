---
title: "Konfiguracja Atom i polecane wtyczki"
description: "Bezpośrednim konkurentem Bracketsa na rynku edytorów dla programistów jest Atom.io — program stworzony przez GitHuba, zapewniający integrację z największym serwisem Git na świecie. Dzisiaj pokażę Ci, jak dobrze skonfigurować Atom."
pubDate: 2017-06-13
tags: 
  - "Junior WebDev"
slug: "konfiguracja-atom"
---

Bezpośrednim konkurentem [Bracketsa](/archive/konfiguracja-brackets/) na rynku edytorów dla programistów jest Atom.io — program stworzony przez GitHuba, zapewniający integrację z największym serwisem Git na świecie. Dzisiaj pokażę Ci, jak dobrze skonfigurować Atom.

<!--more-->

![](images/atom-screenshot.png)

## 1\. Instalacja Atom

Pobierz aplikację ze strony [Atom.io](https://atom.io). Atom instaluje się automatycznie po uruchomieniu instalatora, wystarczy chwila cierpliwości.

## 2\. Konfiguracja

### Telemetria

Możesz zezwolić lub odmówić, aby program wysyłał do twórców dane telemetryczne dotyczące m.in. czasu startu, długości sesji czy błędów.

![](images/atom-telemetry-1024x513.png)

### Wbudowane wtyczki

Wbudowane wtyczki (`Core Packages`) są dostępne w ustawieniach (`[Ctrl] + [,]`) w zakładce `Packages`. Dużą część z nich można wyłączyć, by nie obciążały edytora przy starcie. Możesz na pewno wyłączyć te wtyczki, które dotyczą języków, z których nie korzystasz.

### Motywy

Ta zakładka również znajduje się w Ustawieniach pod nazwą `Themes`. Domyślne ustawienia są moim zdaniem w porządku. Możesz ustawić osobno motyw i podświetlenie składni.

## 3\. Wtyczki

Instalację wtyczek, szablonów i ustawień podświetlania składni polecam dokonywać przez Ustawienia, w zakładce `Install`.

### Emmet

Emmet jest jednym z najpopularniejszych rozszerzeń do webdevu we wszystkich edytorach oraz IDE. Pozwala on na pisanie skrótów selektorów, które automatycznie są konwertowane na pełny kod.

Instalacja: wpisz w wyszukiwarce instalatora _Emmet_ i zainstaluj wtyczkę. Użycie: wpisz skróty selektorów, jakich chcesz użyć i naciśnij `[Tab]`. Pełna lista skrótów: [Emmet Cheat Sheet](https://docs.emmet.io/cheat-sheet/)

### Beautify

Twój kod bardziej przypomina krzak niż pięknie poukładany ciąg linii z odpowiednimi wcięciami? Nic nie szkodzi, Beautify poukłada za Ciebie tabulacje i entery. Tylko następnym razem postaraj się napisać taki kod, do którego nie będzie potrzeba poprawy wielu wierszy ;)

Instalacja: wpisz w wyszukiwarce instalatora _atom-beautify_ i zainstaluj wtyczkę. Użycie: użyj kombinacji `[Ctrl] + [Alt] + [B].`

### File Icons

Tak, jak w przypadku Bracketsa, domyślnie edytor pokazuje jedynie rozszerzenia plików. Dla szybszego rozeznania co to za typ pliku, warto doinstalować wtyczkę _File Icons_.

![](images/atom-file-icons.png)

Instalacja: wpisz w wyszukiwarce instalatora _file-icons_ i zainstaluj wtyczkę.

### Git

Ponieważ Atom jest produktem GitHuba, nie dziwi wbudowana obsługa Gita. Na bieżąco na pasku obok numeru linii widzimy które wiersze zostały zmodyfikowane, a które dodane. W prawym dolnym rogu wyświetlana jest informacja o aktualnej gałęzi (branch) i ilości dokonanych zmian.

![](images/atom-git-diff-1024x513.png)

Jeśli potrzebujesz jeszcze więcej Gita w Atomie, skorzystaj z wtyczki _Git-Plus_, która pozwoli Ci m.in. commitować zmiany bez użycia konsoli, z poziomu Command Palette (`[Ctrl] + [Shift] + [P]`).

Instalacja: wpisz w wyszukiwarce instalatora _git-plus_ i zainstaluj wtyczkę. Użycie: przez Command Palette wybierz odpowiednią opcję.

### Fira Code

Fira Code to font typu monoscape, zaprojektowany do używania w edytorach dla programistów. Jego największą zaletą jest graficzna "ludzkie" podejście do wyświetlania znaków równości, porównań czy większości/mniejszości:

![FiraCode JavaScript](images/firacode-javascript-1024x416.png)

Instalacja: pobierz [paczkę ZIP](https://github.com/tonsky/FiraCode/archive/master.zip) z fontem i zainstaluj go do systemu. Następnie skonfiguruj prawidłowe wyświetlanie znaków w edytorze — zobacz instrukcję pod tym linkiem: [github.com/tonsky/FiraCode/wiki/Atom-instructions](http://github.com/tonsky/FiraCode/wiki/Atom-instructions).

### Minimap

Minimap jest przydatną wtyczką, pokazującą jak mniej więcej zbudowany jest nasz plik (wraz z wcięciami i kolorowaniem składni) oraz w którym jego miejscu jesteśmy. Posiada również wiele innych wtyczek rozszerzających jego możliwości, jak na przykład `minimap-git-diff`, który na minimapie pokazuje dokonane zmiany, które będą miały swoje odzwierciedlenie w commicie na Gita.

![Atom Minimap](images/atom-minimap-1024x513.png)

Instalacja: wpisz w wyszukiwarce instalatora _minimap_ i zainstaluj wtyczkę. Użycie: aby zwinąć lub pokazać minimapę, kliknij w obszarze kodu prawym przyciskiem myszy i wybierz _Toggle Minimap_.

### Pigments

Pigments, podobnie jak Color Highlighter dla Brackets, koloruje użyte w pliku arkusza styli (CSS, LESS, SASS, SCSS, Stylus) wartości lub słowa kluczowe na kolory, które sobą reprezentują.

![Atom Pigments](images/atom-pigments.png)

Instalacja: wpisz w wyszukiwarce instalatora _pigments_ i zainstaluj wtyczkę.

* * *

Tak skonfigurowany Atom pozwala na szybką komfortową pracę z kodem.
