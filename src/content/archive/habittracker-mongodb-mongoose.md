---
title: "#HabitTracker — podstawy MongoDB i Mongoose"
description: "Siema! Wracam z serią o Habit Trackerze. W zeszłym tygodniu usilnie walczyłem z konfiguracją Webpacka (a jakże…) i niedoczasem, stąd luka we wpisach."
pubDate: 2017-08-18
tags: 
  - "Habit Tracker"
  - "Junior WebDev"
slug: "habittracker-mongodb-mongoose"
---

Siema! Wracam z serią o Habit Trackerze. W zeszłym tygodniu usilnie walczyłem z konfiguracją Webpacka (a jakże…) i niedoczasem, stąd luka we wpisach. Dzisiaj podejmuję bardziej teoretycznie temat silnika baz danych, którego będę używać w projekcie — MongoDB.

<!--more-->

Czysta składnia Mongo nie zachęca mnie do korzystania z niej, dlatego do komunikacji z bazą posłużę się biblioteką Mongoose, służącą do modelowania danych i ich egzekwowania.

* * *

Na początku chciałbym przybliżyć jak będzie wyglądać baza danych, na której będę operować:

![Planowanie tabel i kolumn bazy danych](images/003-planning-db-1024x725.jpg)

Prosta baza z dwoma tabelami i małą ilością kolumn wewnątrz. Dobra podstawa, by przejść do tworzenia modeli i pierwszego wstawania danych do bazy.

## Instalacja silnika bazodanowego

Instalki można znaleźć na stronie [MongoDB](https://www.mongodb.com/download-center?ct=false#community). Skorzystamy z wersji _Community Server_. W moim przypadku (Windows 10 x64) wybrałem instalator dla środowiska Windows Server 2008 R2 ze wsparciem dla SSL, co nie ma większego znaczenia biorąc pod uwagę działanie bazy na hoście lokalnym.

![Strona MongoDB z instalatorem bazy danych do pobrania](images/003-mongodb-installer-1024x747.png)

Przy okazji warto również pod Windowsem skonfigurować MongoDB, aby działało jako usługa systemowa. Szczegóły znajdziecie w [dokumentacji](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/#configure-a-windows-service-for-mongodb-community-edition).

## MongoDB Compass

Tak samo, jak możemy zainstalować PHPMyAdmin na serwerze, by obsługiwać MySQL przez interfejs graficzny, tak samo możemy postąpić w przypadku MongoDB. MongoDB Compass dostarcza ten sam podmiot, który tworzy silnik bazodanowy, więc nie powinno być żadnych problemów przy operowaniu danymi za jego pomocą.

![MongoDB Compass — okno programu](images/003-compass-database-1024x641.png)

Program można pobrać [stąd](https://www.mongodb.com/products/compass). Zanim pobierzecie program, będziecie musieli podać swoje dane (m. in. imię, nazwisko i e-mail). Po instalacji, jeżeli masz uruchomiony silnik, włącz program i wpisz domyślne dane (host: localhost, port: 27017).

![MongoDB Compass — połączenie z bazą danych](images/003-compass-connect-1024x641.png)

Po kliknięciu "Connect" powinna ukazać się lista baz danych, a konkretniej jedna o nazwie "local".

## Konfiguracja projektu

Przejdźmy do katalogu, w którym znajduje się projekt. Stwórzmy katalog dla logiki bazy danych oraz zainstalujmy Mongoose, które będzie pośrednikiem między bazą a aplikacją.

```sh
mkdir server
mkdir server/models
npm install mongoose
```

Po instalacji stwórzmy 3 pliki: w katalogu głównym projektu plik `database.js`, na którym będziemy ćwiczyć dodawanie danych, a wewnątrz katalogu `server/models` dwa pliki: `User.js` oraz `Habit.js`.

Zacznijmy od pliku `User.js`:

```javascript
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: { type: String, required: true },
  surname: String,
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  created_at: Date
});
```

W ten sposób tworzymy model, który w Mongoose jest tworzony dzięki interfejsowi Schema. Każda linia to kolumna tabeli, z własnym typem danych oraz informacją czy jest to pole wymagane. **Pamiętajcie o co najmniej szyfrowaniu haseł w prawdziwych aplikacjach, inaczej lepa w twarz od Niewidzialnej Ręki Bezpieczeństwa Danych**. Dla celów treningowych pominiemy ten element, znajdzie się on w późniejszych etapach budowy aplikacji.

W przypadku kolumny `created_at` chcemy, by data utworzenia danego rekordu była wstawiana dynamicznie. W Mongoose służy do tego funkcja `Schema.pre(method, callback)`:

```javascript
userSchema.pre('save', function(next) {
  if (!this.created_at)
    this.created_at = new Date;

  next();
});
```

`next` to `queryCursor` (zobacz [dokumentację](http://mongoosejs.com/docs/api.html#cursor_QueryCursor_QueryCursor-next)), który wskazuje na następny dokument od "kursora".

Mamy schemat tabeli, ale nie mamy samej tabeli. Tabela w Mongoose to model.

```javascript
const User = mongoose.model('users', userSchema);

module.exports = User;
```

Od razu wyeksportujemy też naszą tabelę, dzięki czemu będzie można ją wykorzystać w następnym kroku. Schemat działania dla tabeli `habits` jest taki samo, dlatego pominę go (kod pliku `Habit.js` będzie dostępny pod koniec wpisu).

Przejdźmy do pliku `database.js`, za pomocą którego będziemy dodawać dane do bazy:

```javascript
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/Habit Tracker');
```

Tutaj ważna uwaga: udane połączenie z bazą danych, przy wskazaniu bazy danych jako "podstronę" w linku, powoduje jej utworzenie, jeżeli oczywiście wcześniej nie istniała. Zaimportujmy schemat i model `User` oraz stwórzmy pierwszy rekord do tabeli.

```javascript
const User = require('./server/models/User');

const newUser = new User({
  name: 'Wojtek',
  surname: 'Wernicki',
  email: 'user@domain.tld',
  password: 'qwerty123'
});
```

Nie podaję specjalnie wartości dla kolumny `created_at`, aby jej wartość była przypisana dynamicznie. Zapiszmy nowy rekord do tabeli w bazie:

```javascript
newUser.save((err) => {
  if (err) throw err;

  console.info('User saved successfully!');
});

mongoose.disconnect();
```

Teraz pora na uruchomienie skryptu. W konsoli, będąc w katalogu głównym projektu, uruchom polecenie `node database.js`. Jeżeli wszystko poszło okej, to oprócz ostrzeżeniem o użyciu przestarzałych funkcji i metod (wszystko to zasługa `mongoose.connect()`), w konsoli powinien wyświetlić się komunikat zadeklarowany w `console.info`. Zajrzyj też do Compassa, żeby przekonać się, że nowy rekord znajduje się w bazie i tabeli.

![MongoDB Compass — nowo dodany rekord w tabeli](images/003-compass-record-1024x641.png)

* * *

Na koniec dołączam kod źródłowy trzech plików, które zostały stworzone na potrzeby tego wpisu i prac nad Habit Trackerem.

<script is:inline src="https://gist.github.com/wojciech-space/71ec91090b8e3c399594ce6df6a6ac1b.js"></script>

