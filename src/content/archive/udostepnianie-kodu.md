---
title: "Jak udostępniać swój kod?"
description: "Dzisiaj na warsztat biorę serwisy pozwalające deweloperom udostępnić plik źródłowy lub całą stronę internetową, z łatwym dostępem do kodu źródłowego."
pubDate: 2017-04-26
tags: 
  - "Junior WebDev"
slug: "udostepnianie-kodu"
---

Dzisiaj na warsztat biorę serwisy pozwalające deweloperom udostępnić plik źródłowy lub całą stronę internetową, z łatwym dostępem do kodu źródłowego.

<!--more-->

Jeżeli znacie inne, podobne strony, o których nie wspomniałem poniżej, dajcie o nich znać w komentarzu 😊 **WAŻNA UWAGA!** – ta strona jest ciężka i toporna, bo ładuje kod z 4 różnych źródeł. Dla lepszego komfortu przeczytaj ten wpis na komputerze z wykorzystaniem Wi-Fi.

## [GitHub Gist](https://gist.github.com)

Gist jest usługą pozwalającą na udostępnienie jednorazowo tylko jednego pliku dowolnego typu. Po wpisaniu nazwy pliku edytor rozpozna rozszerzenie i ustawi odpowiednie tabulacje czy kolorowanie składni. Gotowego Gista możemy udostępnić publicznie lub wybrać tryb prywatny, dzięki czemu dostęp do kodu będą mieć jedynie osoby posiadające URL strony.

![Gist — wygląd strony, kod pliku](images/gist.png) 

Dodatkowo Gist, tak jak i inne poniższe serwisy, pozwala na umieszczenie kodu na swojej stronie za pomocą JavaScriptu:

<script is:inline src="https://gist.github.com/wojciech-space/d92b72ded91d1ad83edddf0328bf390e.js"></script>

## [Pastebin](https://pastebin.com/)

Pastebin ma praktycznie te same funkcjonalności co Gist. Jeden "paste" (wklejka) pozwala na wrzucenie kodu tylko jednego pliku. Zaletą jest możliwość ustawienia czasu autodestrukcji naszego kodu, mamy kilka interwałów czasowych do wyboru — od 10 minut do jednego miesiąca.

![Pastebin — wygląd strony, kod pliku](images/pastebin-1024x711.png)

Ogółem Pastebin ustępuje jednak Gistowi pod względem komfortu użytkowania. W darmowej wersji konta umieszczono reklamy, maksymalny rozmiar pliku to 500 kB, a dziennie możemy wrzucić ich jedynie 20.

<script is:inline src="https://pastebin.com/embed_js/5BNkJbU0"></script>

## [JSFiddle](https://jsfiddle.net/)

Przejdźmy do pełnowymiarowych edytorów online. JSFiddle daje nam możliwość edycję HTML, CSS i JavaScript. Niewygodne jest rozwiązanie dodawania zewnętrznych bibliotek CSS, musimy wklejać adres do każdej z nich ręcznie w pasku bocznym po lewej stronie.

![JSFiddle — wygląd i kod strony](images/jsfiddle-1024x561.png)

CSS możemy zastąpić w opcjach SASS-em (konkretnie SCSS) oraz dołożyć do tego normalize.css. W ustawieniach JS-a czeka na nas wiele więcej możliwości wyboru — samych tylko języków mamy cztery, dodatkowo mamy do dyspozycji listę z najpopularniejszymi bibliotekami, m.in. jQuery, Angular i React.

<script is:inline async src="//jsfiddle.net/WojtekWernicki/88cLytct/10/embed/"></script>

## [JSBin](http://jsbin.com)

JSBin na pierwszy rzut oka wygląda ubogo, jednak są to tylko pozory. Jest to pełnoprawny edytor, który niczym nie ustępuje nawet aplikacjom desktopowym.

![JSBin — wygląd i kod strony](images/jsbin-1024x561.png)

Podobnie jak w JSFiddle mamy zakładki dla HTML-a, CSS-a i JS-a, tutaj jednak podobieństwa się kończą. JSBin oferuje wbudowaną konsolę JavaScriptu, co jest pomocne w trakcie debugowania. Dodatkowo twórcy dołożyli od siebie do każdej z zakładek kompilatory. Dzięki temu do HTML-a można przetworzyć Markdown i Jade, do arkuszy styli Less, Myth, Stylus lub S\*SS, a do skryptów m.in. ES6/Babel, React, TypeScript i CoffeeScript.

Warto też poświęcić uwagę ustawieniom konta po rejestracji. Można zarejestrować się przez GitHuba, co polecam ze względu na integrację z Gistem. Jedyną wadą dla darmowego użytkownika jest brak możliwości embedowania edytora na stronie.

## [CodePen](https://codepen.io/)

CodePen to jedyny mocny konkurent dla JSBina pod względem funkcjonalności. Podobnie jak poprzednik, ma podobne możliwości kompilacji styli i skryptów, wygrywa natomiast ilością bibliotek, które można dodać ad hoc z listy w ustawieniach danej strony – nie trzeba ich dodawać ręcznie do sekcji \`\`.

![CodePen — wygląd i kod strony](images/codepen-1024x561.png)

W przeciwieństwie do JSBina, CodePen umożliwia embedowanie kodu strony. Warto też zaglądać na stronę główną serwisu, gdzie można zobaczyć wyróżnione projekty, które powstały właśnie dzięki temu narzędziu.

<p class="codepen" data-height="265" data-theme-id="dark" data-slug-hash="qmNJbz" data-default-tab="result" data-user="WojtekWernicki" data-embed-version="2" data-pen-title="CodePen - przykład do bloga" data-preview="true">See the Pen <a href="https://codepen.io/WojtekWernicki/pen/qmNJbz/">CodePen - przykład do bloga</a> by Wojtek Wernicki (<a href="http://codepen.io/WojtekWernicki">@WojtekWernicki</a>) on <a href="http://codepen.io">CodePen</a>.</p>

<script is:inline async src="https://production-assets.codepen.io/assets/embed/ei.js"></script>
