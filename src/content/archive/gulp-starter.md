---
title: "gulp-starter — open source'owy starter dla projektów front-endowych"
description: "Siema! W tym tygodniu chciałbym zaprezentować Wam mój nowy projekt open source dla programistów front-endowych automatyzujący pracę przy arkuszach stylów i plikach JavaScriptu."
pubDate: 2017-05-23
tags: 
  - "Junior WebDev"
slug: "gulp-starter"
---

Siema! W tym tygodniu chciałbym zaprezentować Wam mój nowy projekt open source dla programistów front-endowych automatyzujący pracę przy arkuszach stylów i plikach JavaScriptu — ~~`gulp-starter`~~ **(UPDATE 30-08-2024: kod nie jest już dostępny)**.

<!--more-->

## Pomysł

Rozpoczynając pracę nad nowym motywem dla bloga oraz tworząc CV dla dziewczyny w HTML-u i CSS-ie (więcej informacji o tym wkrótce) automatyzowałem kompilację i optymalizację arkuszy i skryptów. Za każdym razem sięgałem do internetu po poradnik jakie pakiety gulpa zainstalować, by pomóc sobie w programowaniu. Nadszedł jednak czas, w którym postanowiłem stworzyć własną templatkę, dzięki której oszczędzam czas i zasoby, i wypuściłem ją publicznie na swoim GitHubie jako open source.

![gulp-starter — widok repozytorium plików](images/gulp-starter-github-1024x564.png)

## Jak to działa?

`gulp-starter` jest napędzany przez Node.js, npm oraz gulpa. Po wykonaniu forka repozytorium startera i instalacji pakietów Node'a gulp jest gotowy do użytkowania. Podstawowe elementy tej konfiguracji to:

- Kompilator SASS
- Minimalizacja CSS
- Łączenie (concat) plików JS
- JSHint (dla ES6)
- Optymalizacja obrazów

![gulp-starter — fragment kodu](images/gulp-starter-code.png)

## Błędy

Zdaję sobie sprawę, że ten zestaw nie jest idealny, o czym świadczy chociażby sekcja Q&A w pliku ~~README~~ **(UPDATE 30-08-2024: kod nie jest już dostępny)**. Ponieważ człowiek uczy się na błędach, będę się starał udoskonalić tę konfigurację i wyeliminować problemy, które zauważyłem w trakcie testów.

![gulp-starter — sekcja Q&A pliku README](images/gulp-starter-qa.png)

## Plany na przyszłość

Oprócz wspomnianego wyżej procesu udoskonalania startera, zamierzam również przygotować wersję opartą na grunt.js. Będzie to idealna okazja na porównanie obu task runnerów w akcji i wybranie tego lepszego do dalszej pracy z projektami.

Zachęcam Was do zajrzenia na ~~repozytorium projektu~~ **(UPDATE 30-08-2024: kod nie jest już dostępny)**. Jeśli na sali znajduje się człowiek, który wie co dolega mojemu `gulpfile.js` i chce pomóc, to zapraszam i zachęcam do robienia pull requestów i/lub kontaktu ze mną w wiadomości prywatnej :)

