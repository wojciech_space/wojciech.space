---
title: "Przegląd popularnych usług Git"
description: "Umiejętność korzystania z Gita to jedna z podstawowych cech dobrego programisty. Zarówno w pracy, jak i w prywatnych projektach, jest to dobry sposób na przechowywanie kodu i jego backup. Jednak jaki system kontroli wersji warto wybrać do prywatnego użytku?"
pubDate: 2017-06-06
tags: 
  - "Junior WebDev"
slug: "przeglad-popularnych-uslug-git"
---

Umiejętność korzystania z Gita to jedna z podstawowych cech dobrego programisty. Zarówno w pracy, jak i w prywatnych projektach, jest to dobry sposób na przechowywanie kodu i jego backup. Jednak jaki system kontroli wersji warto wybrać do prywatnego użytku?

<!--more-->

## Podstawowe cechy

Wszystkie niżej wymienione usługi cechuje:

- logowanie dwuskładnikowe
- dostęp do repozytoriów przez SSH (secure shell)
- import repozytoriów z innych źródeł (Git, w zależności od usługi również SVN, Mercurial, Google Code)
- publiczne repozytoria są darmowe

## [GitHub](https://github.com/)

![GitHub - wygląd strony](images/github-1024x484.png)

GitHub jest najpopularniejszą usługą VCS na świecie, z największą społecznością użytkowników i największymi projektami, chociażby Gitem lub jądrem Linuxa. Inną zaletą jest integracja z innymi usługami dla programistów, chociażby z CodePenem, o którym pisałem w tekście ["Jak udostępniać swój kod?"](/archive/udostepnianie-kodu/). Link do GitHuba w Twoim CV lub portfolio z ciekawymi projektami na pewno pomoże Ci w poszukiwaniu pracy programisty.

Oprócz samego systemu kontrolu wersji znajdziemy tutaj usługę hostowania statycznych stron — GitHub Pages, również dostępną z poziomu repozytorium. Przy odrobinie samozaparcia i przeczytaniu dobrego przewodnika możesz dzięki Pages nawet postawić swojego bloga 😉

### Cechy GitHuba:

- nielimitowana wielkość repo (po przekroczeniu 1 GB dostaniesz e-maila z prośbą o "odchudzenie" repozytorium)
- issue tracking (narzędzie służące do śledzenia i informowania o błędach, poprawkach, sugerowanych funkcjach etc.)
- wbudowany kanban (system zarządzania projektem) dla każdego repozytorium
- wbudowana sekcja "Wiki" dla każdego repozytorium
- prywatne repozytoria wyłącznie w pakietach premium

## [BitBucket](https://bitbucket.org/)

![BitBucket - wygląd strony](images/bitbucket-1024x484.png)

Nie samym GitHubem człowiek żyje, a przy okazji to dobrze, że ma on swoją konkurencję. BitBucket jest mocnym konkurentem GH, jeśli chodzi o możliwości repozytoriów. Oferta nielimitowanej ilości prywatnych repo jest bardzo kusząca, ale co za tym idzie, łączna waga plików nie może przekroczyć 2 GB na projekt.

### Cechy BitBucketa

- 2 GB/repozytorium
- nielimitowana ilość prywatnych repozytoriów
- maksymalnie 5 użytkowników na zespół w darmowym pakiecie
- możliwość logowania przez Google

## [GitLab](https://about.gitlab.com/)

![GitLab - wygląd strony](images/gitlab-1024x484.png)

GitLab jest najmniejszą z przedstawionych usług. Osobiście podoba mi się w niej minimalizm interfejsu. Tak samo, jak BitBucket, oferuje nielimitowaną ilość prywatnych repozytoriów za darmo, jednak górny limit pojemności to 10 GB. Miłymi dodatkami jest możliwość dodania plików changeloga i instrukcji dla społeczności jak dokonać kontrybucji z poziomu strony głównej repo oraz ustawienia CI (ciągłej integracji).

Kronikarski obowiązek nakazuje mi wspomnieć o fuckupie, który nastąpił 31.01.2017 — ktoś na bazie danych na produkcji puścił `rm -rf` (więcej informacji [tutaj](https://docs.google.com/document/d/1GCK53YDcBWQveod9kfzW-VCxIABGiryG7_z_6jHdVik/pub)). To pokazuje, że lepiej jest trzymać swoje pliki w kilku miejscach, dla pewności, że w razie awarii u nas lub u usługodawcy nasze dane będą bezpieczne i nienaruszone.

### Cechy GitLaba

- 10 GB/repozytorium
- nielimitowana ilość prywatnych repozytoriów
- nielimitowana ilość użytkowników w ramach repozytorium
- issue tracking (narzędzie służące do śledzenia i informowania o błędach, poprawkach, sugerowanych funkcjach etc.)
- wbudowana sekcja "Wiki" dla każdego repozytorium
- możliwość skonfigurowania GitLaba na własnym serwerze
- nikt na produkcji już nie wpisze `rm -rf` 😉
