---
title: "Gulp — podstawowa konfiguracja task runnera"
description: "Rozpoczynam krótką, trzyczęściową serię wpisów dotyczących automatyzacji pracy w web developmencie. W tym tygodniu zajmę się konfiguracją gulp, za tydzień – grunta, a za dwa – porównaniem obu task runnerów i wybraniem tego lepszego."
pubDate: 2017-06-27
tags: 
  - "Junior WebDev"
slug: "konfiguracja-gulp"
---

Rozpoczynam krótką, trzyczęściową serię wpisów dotyczących automatyzacji pracy w web developmencie. W tym tygodniu zajmę się konfiguracją gulp, za tydzień – grunta, a za dwa – porównaniem obu task runnerów i wybraniem tego lepszego.

Poniżej znajdziesz instrukcję jak skonfigurować gulp w sposób minimalny — to jest skompilować SASS, zminimalizować i połączyć ze sobą pliki oraz skompresować zdjęcia. Tak przygotowany zestaw szybko zainstalujesz i dostosujesz do swoich potrzeb.

<!--more-->

## 0\. Instalacja Node.js

Jeśli jeszcze nie masz na komputerze Node.js, zainstaluj go teraz. Polecam na początek zainstalować stabilną wersję 6.11.0, która ma LTS (Long Time Support). Link do strony: https://nodejs.org/en/

## 1\. Wymagania do web developmentu

Określmy, do czego użyjemy gulpa. We front-endzie zależy nam przede wszystkim na minimalizacji wagi plików oraz szybkości ładowania stron. Arkusze styli będą powstawać w SCSS.

Podstawowe zadania:

- kompilacja arkusza styli z SCSS do CSS
- łączenie plików JavaScript w jeden plik
- minimalizacja arkusza styli i skryptów
- optymalizacja zdjęć (kompresja)

## 2\. Struktura plików

Na samym początku stwórz dwa główne katalogi: `src` oraz `dist`. Wewnątrz `src` będziesz umieszczać surowe dane, które dopiero będą przetwarzane, a w folderze `dist` znajdziesz pliki gotowe do użycia po kompilacji/minifikacji. Wewnątrz `src` stwórz następujące foldery: `scss`, `js`, `img`, a wewnątrz `dist`: `css`, `js`, `img`.

![Struktura plików projektu — konfiguracja gulp](images/gulp-dir.png)

## 3\. Instalacja

Otwórz konsolę i przejdź w niej do katalogu, w którym znajduje się projekt. Na samym początku, jeśli nigdy wcześniej nie korzystałeś z gulpa, zainstaluj go globalnie:

```sh
npm install -g gulp gulp-cli
```

Możesz teraz zainicjalizować Node.js oraz npm w folderze za pomocą komendy `npm init`. W trakcie inicjalizacji npm spyta o kilka informacji, możesz je przeklikać naciskając Enter. Po chwili zostanie utworzony plik package.json z podstawowymi informacjami o projekcie (nazwa, wersja, adres repozytorium etc.).

Następnie możesz przystąpić do instalacji potrzebnych paczek, których użyjesz w task runnerze:

```sh
npm install --save-dev gulp gulp-sass gulp-clean-css gulp-concat pump gulp-uglify gulp-imagemin gulp-rename
```

## 4\. Konfiguracja pliku

Po instalacji paczek utwórz w katalogu głównym projektu plik `gulpfile.js`, w którym będzie znajdowała się konfiguracja gulpa.

Na początku pliku "zaimportuj" wszystkie paczki, które zainstalowałeś:

```javascript
const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const pump = require('pump');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
```

### Arkusze styli (SCSS → CSS)

Na początku skonfiguruj kompilator SASS. W tym celu użyjesz trzech paczek: `gulp-sass`, `gulp-clean-css` oraz `gulp-rename`. Pod importem paczek stwórz nowy task dotyczący arkuszy styli:

```javascript
gulp.task('sass', function() {
    return gulp.src('src/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('dist/css'))
    .pipe(cleanCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist/css'))
});
```

Deklaracja zadania jest prosta. Funkcja `gulp.task()` przyjmuje dwa argumenty: nazwę, po której może być wywołana oraz:

- funkcję, która zostanie uruchomiona po wywołaniu zadania,
- tablicę zawierającą nazwy zadań, które mają zostać uruchomione.

Tablica zadań przyda nam się później, gdy będziemy chcieli zautomatyzować pracę gulpa.

Wewnątrz `gulp.src` znajduje się katalog oraz pliki, które mają być objęte danym taskiem. `.pipe` służy do przekazywania kolejnych poleceń gulpa — w tym przypadku, w kolejności, będą to: kompilacja arkusza, skopiowanie go do katalogu `dist` (odpowiada za to funkcja `gulp.dest`), minimalizacja pliku (za pomocą zaimportowanej funkcji `gulp-clean-css`), zmiana nazwy zminimalizowanego pliku poprzez dodanie suffixu `.min`, zapisanie tego pliku w tej samej lokalizacji co skompilowany arkusz.

### JavaScript

Pora na ECMAScript. W tym przypadku będziemy łączyć wszystkie pliki JS w jeden — `gulp-concat`, minimalizować pliki przy pomocy `gulp-uglify` i `pump` oraz zmieniać jego nazwę dzięki `gulp-rename`.

```javascript
gulp.task('concat', function() {
    return gulp.src('src/js/*.js')
    .pipe(concat('script.js'))
    .pipe(gulp.dest('dist/js'))
});

gulp.task('compress', function(cb) {
    pump([
        gulp.src('dist/js/script.js'),
        uglify(),
        rename({suffix: '.min'}),
        gulp.dest('dist/js')
    ], cb);
});
```

Dlaczego oddzielnie łączenie pliki i minimalizacja? `gulp-uglify` w połączeniu z `.pipe()` w moich konfiguracjach powodował błędy, które uniemożliwiały prawidłowe łączenie plików w jeden i ich minimalizację. Sposób przedstawiony powyżej, zgodny z [dokumentacją](https://www.npmjs.com/package/gulp-uglify) `gulp-uglify`, zapewnia pomyślne wykonanie operacji. Nie należy się przejmować dodatkowym taskiem w konfiguracji, problem zostanie rozwiązany w sekcji **Automatyzacja**.

### Optymalizacja zdjęć

Zadanie dotyczące komprezji obrazków jest najmniejsze, mieści się w pięciu liniach. Wykonywane jest przez paczkę `gulp-imagemin`.

```javascript
gulp.task('images', function() {
    return gulp.src('src/img/*.*')
    .pipe(imagemin({optimizationLevel: 7, progressive: true}))
    .pipe(gulp.dest('dist/img'));
});
```

### Automatyzacja

Każdy z tasków można uruchomić w konsoli samodzielnie, za pomocą komendy `gulp <<nazwa_taska>>`. Na dłuższą metę jednak potrzebujemy automatyzacji, żeby taski wykonywały się same, gdy zajdą zmiany w plikach. Do takich działań służy funkcja `gulp.watch`. Przyjmuje ona dwa argumenty — lokalizację i pliki, które ma obserwować oraz funkcje, które mają być wywoływane, gdy zapiszemy zmiany w plikach.

```javascript
gulp.task('watch', function() {
    gulp.watch('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/js/*.js', ['concat']);
    gulp.watch('dist/js/*.js', ['compress']);
    gulp.watch('src/img/*.*', ['images']);
});
```

### Zadanie domyślne

Zadanie domyślne w gulpie to task o nazwie "default". Task w tym przypadku jako drugi argument przyjmuje tablicę funkcji, które mają się uruchomić przy wpisaniu w konsolę `gulp`.

```javascript
gulp.task('default', ['sass', 'concat', 'compress', 'images', 'watch']);
```

Cały plik możesz skopiować stąd:

<script is:inline src="https://gist.github.com/wojciech-space/0d2c05bc4ae76f3d4aa47838a31cba74.js"></script>

Ten wpis jest jedną z części mini serii dotyczącej task runnerów. Zobacz pozostałe wpisy:

1. **Gulp — podstawowa konfiguracja task runnera**
2. [Grunt — podstawowa konfiguracja task runnera](/archive/konfiguracja-grunt)
3. [Gulp vs Grunt — porównanie task runnerów](/archive/gulp-grunt-porownanie)
