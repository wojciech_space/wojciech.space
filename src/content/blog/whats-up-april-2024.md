---
title: "What's up? April 2024"
description: "Here's some of the things I did since my last post and few interesting articles that I recently read."
tags: ["Programming", "Side Project", "Personal"]
pubDate: 2024-04-26
slug: "whats-up-april-2024"
---

My last post here was on July 30, 2023. Long time ago. Since then, a lot of changed in my life, and I'm not even exaggerate.

Some of my passions had to make a space to accommodate taking care of my partner, who got lymphoma. Every third week we get up at 4 am to get to the hospital for her chemo. It makes the rest of my week unable to do things I would normally do, in terms of working on side projects, learning new things etc.

Although it's still an ongoing process, I finally managed to find a space in my head just for frontend development, finding and learning new cool things etc.

## Development

Last year I discovered [Astro](https://astro.build), which was a big gamechanger for me. Thanks to it, I made few projects:

### This blog

After some malfunction that I didn't quite understand, Ghost stopped working for me. This was a month or two after I discovered Astro, I even had my page ready in ~90%, so the luckiest dev, I just replaced CMS with this SSG page.

### Czy dziś handlowa? (Is Shopping Sunday Today?)

In 2018 Polish Parliament introduced the bill prohibiting shopping in Sunday (with more than a few exceptions), since 2020 only 7 sundays are declared as shopping sundays. My page was created in response to enshittification of search engines results and articles regarding that topic. Idea was simple: to put simple answer YES/NO with bold font in the center of the screen.

- Site URL: https://czydzishandlowa.pl
- GIT Repo: https://codeberg.org/wojciech_space/czydzishandlowa.pl

### about.wojciech.space

I believe in simple design. On the other hand, I don't know how to design. This doesn't matter when the only objective is to put links to places where you can find me online.

- Site URL: https://about.wojciech.space
- GIT Repo: https://codeberg.org/wojciech_space/about.wojciech.space

### Pending

Currently I'm in the process of migration my Polish blog about books, movies and culture [Pełna Kulturka](https://pelnakulturka.art) also from Ghost CMS to Astro.

## Cool articles

In month review posts on my Polish blog I add a section for links to the articles that I recently read and can recommend. I would like to do the same here, this time for tech/dev related things:

- ["Front-end development’s identity crisis" by Elly Loel](https://www.ellyloel.com/blog/front-end-development-s-identity-crisis/)
- ["How to send progress updates" by Slava Akhmechet](https://www.spakhm.com/updates-howto)
- ["The process.env frontend time bomb (plus: a sustainable definition of “fixed”)" by Massimiliano Mirra](https://massimilianomirra.com/notes/the-frontend-process-env-time-bomb-plus-a-sustainable-definition-of-fixed)
- ["HTML attributes vs DOM properties" by Jake Archibald](https://jakearchibald.com/2024/attributes-vs-properties/)

---

Thanks for tuning in, I hope to see you soon! 🧡
