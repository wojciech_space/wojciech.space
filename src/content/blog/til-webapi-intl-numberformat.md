---
title: "#TIL WebAPI: Intl.NumberFormat"
description: "Recently I found very handful function for formatting numbers, currency or units — Intl.NumberFormat, which formats values for given language and region."
tags: ["JavaScript", "Programming", "Today I Learned"]
pubDate: 2022-11-09
slug: "til-webapi-intl-numberformat"
---

Recently I found very handful function for formatting numbers, currency or units — Intl.NumberFormat, which formats values for given language and region.

> The Intl object is the namespace for the ECMAScript Internationalization API, which provides language sensitive string comparison, number formatting, and date and time formatting. The Intl object provides access to several constructors as well as functionality common to the internationalization constructors and other language sensitive functions.[^1]

Recently I found very handful function for formatting numbers, currency or units — `Intl.NumberFormat`, which formats values for given language and region.

Documentation: [Intl.NumberFormat on MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat)

**Sources:**

- Image of JavaScript logotype used in thumbnail is licensed on [MIT license](https://github.com/voodootikigod/logo.js/blob/master/LICENSE), author: [Christopher Williams](https://github.com/voodootikigod).

[^1]: [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl)
