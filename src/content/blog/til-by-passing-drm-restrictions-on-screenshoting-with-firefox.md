---
title: "#TIL By-passing DRM restrictions on screenshoting with Firefox"
description: "Firefox on Windows allows to take a screenshot of DRM-protected media."
tags: ["Today I Learned"]
pubDate: 2022-11-13
slug: "til-by-passing-drm-restrictions-on-screenshoting-with-firefox"
---

Firefox on Windows allows to take a screenshot of DRM-protected media. All you need to do is to have a window with DRM-protected media active and use keyboard shortcut [Alt]+[Print Screen]. Here's an example from DRM-protected livestream:
