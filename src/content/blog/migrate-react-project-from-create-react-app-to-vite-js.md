---
title: "Migrate React project from create-react-app to Vite.js"
description: "Recently I had to replace create-react-app with Vite.js in one project at work. This is a thread of what I did to make that work"
tags: ["Vite.js", "Programming", "JavaScript", "React"]
pubDate: 2023-07-30
slug: "migrate-react-project-from-create-react-app-to-vite-js"
---

Recently I had to replace create-react-app with Vite.js in one project at work. This is a thread of what I did to make that work.

## Remove CRA & install Vite.js

This is pretty straight-forward, I uninstalled CRA and installed Vite.js packages. I followed an example for react-ts available at Vite documentation[^1].

## Add support for tests (Jest, React Testing Library)

That one was tricky, but thanks to 2 articles[^2][^3] on Medium (I will link to privacy-friendly versions) I managed to run tests.

## Vite config

95% of config was easy to set up, the only thing I had to search online was how to force Vite into using HTTPS with some sort of certificate. It was possible with vite-plugin-basic-ssl[^4].

## All set up!

It took me about an hour to set up Vite.js into desired configuration to allow that specific React+TypeScript project into running. I hope it will also help you to kickstart your migration CRA => Vite.

[^1]: https://vitejs.dev/guide/#trying-vite-online
[^2]: https://scribe.rip/quick-jest-setup-with-vitejs-react-typescript-82f325e4323f
[^3]: https://scribe.rip/how-to-setup-jest-and-react-testing-library-in-vite-project-2600f2d04bdd
[^4]: https://github.com/vitejs/vite-plugin-basic-ssl