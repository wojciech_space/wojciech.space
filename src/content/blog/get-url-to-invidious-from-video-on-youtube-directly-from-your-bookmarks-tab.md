---
title: "Get URL to Invidious from video on YouTube, directly from your bookmarks tab"
description: "If you are currently watching video on YouTube and want to get a shareable link to not-so-spying Invidious instance, there is a very simple solution, that can live rent free in your bookmarks tab."
tags: ["JavaScript", "Programming"]
pubDate: 2023-01-05
slug: "get-url-to-invidious-from-video-on-youtube-directly-from-your-bookmarks-tab"
---

If you are currently watching video on YouTube and want to get a shareable link to not-so-spying Invidious instance, there is a very simple solution, that can live rent free in your bookmarks tab.

In my case, I use Firefox, I put this piece of code into "URL" input of the bookmark[^1]:

```javascript
javascript:navigator.clipboard.writeText(`https://yewtu.be/watch?v=${window.location.search.split("&")[0].slice(3)}`)
```

![firefox-bookmark](https://wojciech.space/content/images/2023/01/firefox-bookmark.png "Screenshot of Firefox modal for adding/editing a bookmark.")

In result, privacy friendly link is saved directly in your clipboard, ready to be shared with your friends and other people.

This JavaScript bookmark can be altered and used for other websites, e.g. for creating links redirecting to Nitter, where you can see tweets from Birdsite.

Please let me know in the comments if this solutions works for you on Chromium-based and Safari browsers.

**Sources:**

- [Invidious logo](https://github.com/iv-org/documentation/blob/master/docs/images/invidious.png) used in thumbnail, licensed on [CC0-1.0 license](https://github.com/iv-org/documentation/blob/master/LICENSE)

[^1]: [List of available Invidious instances](https://docs.invidious.io/instances/#list-of-public-invidious-instances-sorted-from-oldest-to-newest)