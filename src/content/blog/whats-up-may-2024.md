---
title: "What's up? May 2024"
description: "
Plans! I love planning! So here's some of them."
tags: ["Programming", "Side Project", "Personal"]
pubDate: 2024-06-02
slug: "whats-up-may-2024"
---

Plans! I love planning! So here's some of them.

## Development

### Pełna Kulturka

My primary blog about books, currently powered by Ghost CMS, is due to be replaced with my own solution, utilizing Markdown and SSG with Astro. I have to make final decisions regarding UX, then recreate all of existing post thumbnails. It will be finished in the end of June.]

### Cloud migration

Just setting up all the necessary services at new provider — Contabo. TBH the main reason is they're cheaper.

## Cool articles

- [Modern web bloat means some pages load 21MB of data - entry-level phones can't run some simple web pages, and some sites are harder to render than PUBG" by Christopher Harper/Tom's Hardware](https://www.tomshardware.com/tech-industry/modern-web-bloat-means-some-entry-level-phones-cant-run-simple-web-pages-and-load-times-are-high-for-pcs-some-sites-run-worse-than-pubg)
- ["How and why to make a /now page on your site" by Derek Sivers](https://sive.rs/now2)

---

Thanks for tuning in, I hope to see you soon! 🧡
