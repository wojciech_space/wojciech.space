---
title: "2022, as it was"
description: "Not gonna lie, this was a tough, challenging year in my personal life. On the other hand, I've never been happier than in 2022."
tags: ["Personal"]
pubDate: 2023-01-02
slug: "2022-as-it-was"
---

Not gonna lie, this was a tough, challenging year in my personal life. On the other hand, I've never been happier than in 2022.

## Personal life

Starting 2022 with whopping 140 kgs on the scale, two anti-depressants and ongoing therapy wasn't the best starting point as you can imagine. 12 months later, 15 kgs less and after the therapy, life is so good right now. I hope nothing will break that.

How I lost these 15 kgs? Cutting off take-aways and walking more. Today I eat less than 1 or 2 years ago and walk between 10k-12k steps a day. I feel that change as exercising is less painful, also most of my t-shirts suits me well again.

My relationship also grows on daily basis. I believe this is my first and last healthy, lovely and inspiring relationship in my life 🤞

The biggest changes for me were a vasectomy and cutting off toxic social media. Vasectomy was a very simple procedure that will keep me far away from having children, as we both don't want them no matter what.

I left Twitter shortly after the vasectomy itself, because, as one meme said, my tweet about that left my target audience. I got so much hate that no one can really handle that amount of shit, all because I don't want have children and I'm using contraceptive.

Last week I also deleted Facebook and Instagram. There were no hate, just feel out of this bubble.

In general, being more healthy, in a supportive relationship filled with love, and discovering what is good and wrong for me, now I'm getting happier every day.

## Books

I read 28 books in 2022, that's pretty good result. All the books were in Polish, most of them I read on Kindle. Personally for e-books I use Polish subscription service Legimi, which has a library of over 160 000 positions. For my Polish folks, the list of read books are here on GoodReads (one book missing at the time of writing this post, I'll add the missing one if I won't forget to do it).

## Programming

Definitely the year of stagnation. I didn't learn anything new, focusing myself on mental health. Also no new side projects, apart from setting up my own Mastodon and Pixelfed instances, thanks to Yunohost.

## What's for 2023?

For sure, I will keep on walking and losing body weight. Kinda sounds funny, but I want to lose 23 kgs in '23 😅 From my work and programming perspective, I'm gonna learn TypeScript and Java or Python, just to be able to understand the processes and logic that exists in other sections of project I'm currently involved. This list also includes creating open source projects, one of which I've already started, with TypeScript and React.

From soft skills, I'm learning this year two languages: Ukrainian and German. From 2022 I have almost 300 days streak learning Ukrainian on Duolingo and I want to get B1 level certificate. With German I have some sort of love-hate relationship, as I always wanted to learn that, but at some point I was just leaving that passion behind me.

I have other plans, some of them including photography and video making (mainly on TikTok, from time to time vlogs posted on YouTube, maybe some tutorials in the future? Who knows…).

I wish everyone happy new year and all the best in 2023 🧡