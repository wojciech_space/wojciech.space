---
title: "#TIL How to connect into MongoDB in Node.js app with Mongoose"
description: "Last week, I was learning MongoDB basics with Mongoose. I highly recommend the course linked below."
tags: ["Node.js", "Today I Learned", "JavaScript"]
pubDate: 2023-05-23
slug: "til-how-to-connect-into-mongodb-in-node-js-app-with-mongoose"
---

Last week, I was learning MongoDB basics with Mongoose. I highly recommend the course linked below.

- [Invidious](https://yewtu.be/playlist?list=PL4cUxeGkcC9jsz4LDYc6kv3ymONOKxwBU)
- [YouTube](https://youtu.be/playlist?list=PL4cUxeGkcC9jsz4LDYc6kv3ymONOKxwBU)
