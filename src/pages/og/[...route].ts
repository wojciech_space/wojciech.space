import { getCollection } from "astro:content";
import { OGImageRoute } from "astro-og-canvas";

const blogs = await getCollection("blog");
const archives = await getCollection("archive");

const staticPages = [
    { slug: "index", title: "wojciech.space", description: "Programming blog about JavaScript, React and more" },
    { slug: "about", title: "About", description: "Who am I?" },
    { slug: "blog", title: "Blog posts", description: "All the posts from wojciech.space" },
    { slug: "uses", title: "/uses", description: "What are my daily essentials?" },
    { slug: "now", title: "/now", description: "What am I doing right now?" },
    { slug: "archive", title: "Archiwum wojtekwernicki.eu", description: "Posty ze zarchiwizowanego bloga wojtekwernicki.eu" },
];

const pages = {
    ...Object.fromEntries(
        blogs.map(({ id, slug, data }) => [id, { data, slug }]),
    ),
    ...Object.fromEntries(
        archives.map(({ id, slug, data }) => [id, { data, slug }]),
    ),
    ...Object.fromEntries(
        staticPages.map(({ slug, ...rest }) => [slug, { slug, data: { ...rest } }]),
    )
};

export const { getStaticPaths, GET } = OGImageRoute({
    param: "route",
    pages,
    getImageOptions: async (_, { data }: (typeof pages)[string]) => {
        return {
            title: data.title,
            description: data.description,
            dir: "ltr",
            border: { color: [255, 102, 0], width: 20, side: "inline-start" },
            bgGradient: [
                [254, 254, 254],
            ],
            fonts: ["./src/fonts/Lato-Regular.ttf"],
            font: {
                title: {
                    color: [51, 51, 51],
                },
                description: {
                    color: [51, 51, 51],
                    size: 32,
                },
            },
            logo: {
                path: "./src/images/og-logo.png",
                size: [475, 128],
            },
        };
    },
})