import rss from "@astrojs/rss";
import { getCollection } from "astro:content";

export async function GET(context) {
    const blog = await getCollection("blog");
    const archive = await getCollection("archive");

    const blogPosts = blog.map((blogPost) => ({
        title: blogPost.data.title,
        description: blogPost.data.description,
        pubDate: blogPost.data.pubDate,
        link: `/${blogPost.slug}`,
        customData: "<language>en-gb</language>"
    }));
    const archivePosts = archive.map((archPost) => ({
        title: archPost.data.title,
        description: archPost.data.description,
        pubDate: archPost.data.pubDate,
        link: `/archive/${archPost.slug}`,
        customData: "<language>pl</language>"
    }));
    return rss({
        title: "Wojciech Wernicki",
        description: "Programming blog about JavaScript, React and more",
        site: context.site,
        items: [...blogPosts, ...archivePosts],
    });
}
